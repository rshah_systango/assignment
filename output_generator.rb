require_relative 'input_processor.rb'
require 'csv'
require 'pry'

class OutputGenerator 
    def initialize(input_processor)
      @input_processor=input_processor
     end      
    def write_output
      @input_processor.get_processing_result.each do |result|
        print result
        puts ''
      end
    end
end  
