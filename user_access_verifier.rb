require_relative 'input_reader'
require_relative 'user_record_initializer'
require_relative 'input_processor'
require_relative 'user_record_container.rb'	
require_relative 'output_generator'
require_relative 'input_data_verification'  	
class UserAccessVerifier
	def initialize (user_data_filename,input_file_name)
      @input_file_name=input_file_name
      @user_data_filename=user_data_filename
    end
	
	def process
		user_data_reader=InputReader.new(@user_data_filename)
		user_record_initializer=UserRecordInitializer.new(user_data_reader)
		input_reader=InputReader.new(@input_file_name)
		input_processor=InputProcessor.new(input_reader)
		output_generator=OutputGenerator.new(input_processor)
		user_data_reader.read_file
		user_record_initializer.populate_user_data
		input_reader.read_file
		input_processor.process_input_data
		output_generator.write_output
	end	
end	